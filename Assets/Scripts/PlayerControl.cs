﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{

	public Button CurrentCard;       // the card is open in th middle
	public Button Top;             // this is the card that on the top has white sprite and gets top nwhen some one win 

	public Text TurnTxt;          // this tells whosee turn is at the moment                 

	public static bool Turn;      // if turn is false means it is pc's turn otherwise it is players turn
	public static bool Wait;      // this waits player during pc plays
	bool FirstCard;                     // this is very first card when game is started

	public static char CurrentCardValue; // this is the value of the current card it might be form A,1,2,....K 

	int PlayerMove;       // this controls how many time Player has played





	// Use this for initialization
	void Start()
	{
		Turn = true;
		Wait = true;
		FirstCard = true;
		PlayerMove = 0;
	}

	// Update is called once per frame
	void Update()
	{

		if (FirstCard)
		{
			CurrentCardValue = SuffleCards.cards[11].ToString()[0];    // this is very first card when game is started
			FirstCard = false;

		}

		//	if (Wait)
		//	{
		//    StartCoroutine(Example());
		//		Wait = false;
		//	}
		if (Turn && Wait)
		{

			TurnTxt.text = "Your Turn";
		}
		else
			TurnTxt.text = "Your Opponent's Turn";  // tels players whoos turn

		if (DistributeCards.HandCount >= 44)
		{

			if (ScoreControl.playerScore > ScoreControl.PCScore)   // shows Who won
			{

				TurnTxt.text = "Player Won";
			}
			else
				TurnTxt.text = "PC Won";
		}

	}

	public void isClicked(Button btn)
	{
		//if (/*Get point*/)
		//{

		//}
		//else                                               // for this Piece of code there is  the same explanation in the OpponentControl.CS
		if (Turn && Wait)
		{
			btn.transform.position = CurrentCard.transform.position;
			btn.transform.parent = CurrentCard.transform;
			Wait = false;
			if (CurrentCardValue == btn.GetComponent<Image>().sprite.name[0])
			{
				if (CurrentCardValue == 'J')
				{
					ScoreControl.playerScore += 20;
					foreach (var item in ScoreControl.CardsOnTable)
					{
						ScoreControl.PlayerEarnedCards.Add(item);
					}
					ScoreControl.CardsOnTable.Clear();
					ScoreControl.LastEarned = true;

				}
				else
				{
					ScoreControl.playerScore += 10;
					foreach (var item in ScoreControl.CardsOnTable)
					{
						ScoreControl.PlayerEarnedCards.Add(item);
					}
					ScoreControl.CardsOnTable.Clear();
					ScoreControl.LastEarned = true;

				}
				Top.gameObject.SetActive(true);
				Top.gameObject.transform.SetSiblingIndex(CurrentCard.transform.childCount);


			}
			else if (btn.GetComponent<Image>().sprite.name[0] == 'J')
			{
				foreach (var item in ScoreControl.CardsOnTable)
				{
					ScoreControl.PlayerEarnedCards.Add(item);
				}
				ScoreControl.CardsOnTable.Clear();
				ScoreControl.LastEarned = true;

				Top.gameObject.SetActive(true);
				Top.gameObject.transform.SetSiblingIndex(CurrentCard.transform.childCount);
			}
			else
			{

				CurrentCardValue = btn.GetComponent<Image>().sprite.name[0];
				DistributeCards.CurrentCardName = btn.GetComponent<Image>().sprite.name;
				ScoreControl.CardsOnTable.Add(DistributeCards.CurrentCardName);

				Top.gameObject.SetActive(false);
			}
			StartCoroutine(Example());

		}


		PlayerMove++;

	}

	IEnumerator Example() // waits two second for Pc to play
	{

		yield return new WaitForSeconds(2f);   
		Turn = false;
		Wait = true;

	}
}
