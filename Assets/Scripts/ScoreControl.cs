﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//  here is where I calculate,show and manage the score 
public class ScoreControl : MonoBehaviour
{


	public static List<string> CardsOnTable = new List<string>();                
	public static List<string> PlayerEarnedCards = new List<string>();     
	public static List<string> PCEarnedCards = new List<string>();

	public Text PlayerScoreTxt;
	public Text PCScoreTxt;

	public static int playerScore;
	public static int PCScore;
	int playerCardCount;                     // this helps to get score of the cards count winner

	public static bool LastEarned;          // this will decide that who will get the cards has been left after all cards allocated  
	bool flag;                             // flag helps me call update function when ever I want


	// Use this for initialization
	void Start()
	{
		playerCardCount = 0;  // in the start everyone has 0 card and Update method will work
		flag = true;
	}

	// Update is called once per frame
	void Update()
	{

		if (DistributeCards.HandCount >= 44 && flag == true)   // only works is cards are finised
		{
			if (LastEarned)     // which means that player won the last cards
			{
				foreach (var item in ScoreControl.CardsOnTable)
				{
					ScoreControl.PlayerEarnedCards.Add(item);
				}
				ScoreControl.CardsOnTable.Clear();
			}
			else          // which means that PC won the last cards
			{
				foreach (var item in ScoreControl.CardsOnTable)
				{
					ScoreControl.PCEarnedCards.Add(item);
				}
				ScoreControl.CardsOnTable.Clear();
			}
			foreach (var item in PlayerEarnedCards)  // to count the player cards and give point for special cars such as 2 club
			{
				if (item == "2C")
				{
					playerScore += 2;
				}
				if (item == "10D")
				{
					playerScore += 3;
				}
				if (item[0] == 'A' || item[0] == 'J')
				{
					playerScore += 1;
				}
				playerCardCount++;
			}
			foreach (var item in PCEarnedCards)  // to count the PC cards and give point for special cars such as 2 club
			{
				if (item == "2C")
				{
					PCScore += 2;
				}
				if (item == "10D")     // Icould  write them above couse if player has nop theese special car so pc have them yer 
				{                      // I have written here to see every move
					PCScore += 3;
				}
				if (item[0] == 'A' || item[0] == 'J')
				{
					PCScore += 1;
				}
				if (playerCardCount < 26)
				{
					PCScore += 3;
				}
				else
					if (playerCardCount > 26)
				{
					playerScore += 3;
				}
			}
			//TurnTxt.text = "GameOver";
			PCScoreTxt.text = "PC=(" + PCScore.ToString() + ")";
			PlayerScoreTxt.text = "Player=(" + playerScore.ToString() + ")";   // after game is over scores will be shown
			flag = false;
		}
	}
}
