﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuffleCards : MonoBehaviour {

	public static Card[] cards = new Card[52];   // the card array is static couse ı will be using it in many classes

	string[] numbers = new string[] {"A", "2", "3", "4", "5", "6", "7", "8", "9","10", "J", "Q", "K" }; // my values


	// Use this for initialization
	void Start ()
	{
		AddCardsToList(); // All cards Will be added to a Card Type List
	}	


	public void Suffle(Card[] array)
	{
		
			System.Random r = new System.Random();
			for (int i = array.Length; i > 0; i--)
			{
				int j = r.Next(i);                             //All cards Will be suffeled ones in the begining
				Card k = array[j];
				array[j] = array[i - 1];
				array[i - 1] = k;
			}
		
		
	}
	public void AddCardsToList()
	{

		int i = 0;
		foreach (string s in numbers)
		{
			cards[i] = new Card(Suits.C, s);    //13 card in Club suit
			i++;

		}
		foreach (string s in numbers)
		{
			cards[i] = new Card(Suits.S, s);         //13 card in Spade suit
			i++;

		}
		foreach (string s in numbers)
		{ 
			cards[i] = new Card(Suits.H, s);             //13 card in Heart suit
			i++;

		}
		foreach (string s in numbers)
		{
			cards[i] = new Card(Suits.D, s);              //13 card in Diamond suit
			i++;


		}
		Suffle(cards);	 // ading cards and suffeling
	}

	

public Card[] Cards{get{return cards;}}}

public class Card   //   I  needed A Card Type Class to define my cards So I ve Crteated it 
{
	protected Suits suit;    // which takes 4 type suit
	protected string cardvalue;  // and 13 type value
	public Card() { }               // didnt use this one yet its nice to have a consructor without any parametr

	public Card(Suits suit2, string cardvalue2)      //Here is a consructor Gets My parametr
	{
		suit = suit2;
		cardvalue = cardvalue2;
	}
	public override string ToString()
	{
		return string.Format("{0}{1}", cardvalue, suit);   //I Need to override ToString method in order to get my cards Writen as  Suit and Value
	}
}



public enum Suits                //there are four suits in the dack 
{
	S,  // Spade
	H,   //Hearth
	D,  //Diamond
	C    //Club
}