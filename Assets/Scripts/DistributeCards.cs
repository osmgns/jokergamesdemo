﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistributeCards : MonoBehaviour
{

	public Button[] PlayerCard;  // where player's current cards are hold
	public Button[] PCCard;  // where Pc's current cards are hold

	public Button CurrentCard;  // where current card is hold

	Sprite Sprites;  // helps tho change the top card when both player fisnishes their cards

	public Text DebugText;  // to show the Logs

	public static bool IsStarted; // Controls if start butoon is clicked

	public static int HandCount;  // // controls that how many hads are played

	public static string CurrentCardName; //hold currnt card's full name



	// Use this for initialization
	void Start()
	{
		HandCount = 0; // no hand played at the very begining

	}

	// Update is called once per frame
	void Update()
	{
		if (HandCount <= 44) // if cards number is not bigger then 44+ 8  (8 cards for the last hand)
		{

			if (IsStarted)  // there is no animation so this part demosrate like there is an animation and positioning
			{ 

				if (PlayerCard[0].transform.position.y <= 70)
				{
					for (int i = 0; i < 4; i++)   // four cards for player
					{
						PlayerCard[i].transform.Translate(Time.deltaTime * (40 * i - 240), Time.deltaTime * 150, 0);

					}


				}
				else
				{
					IsStarted = false;
				}

				if (PCCard[0].transform.position.y >= 330)  // four cards for PC
				{
					for (int i = 0; i < 4; i++)
					{
						PCCard[i].transform.Translate(Time.deltaTime * (40 * i - 240), Time.deltaTime * -150, 0);

					}

				}
				for (int i = 0; i < 4; i++) // fills players cards with the same name sprites
				{
					Sprites = Resources.Load<Sprite>("Cards\\" + SuffleCards.cards[HandCount + i].ToString());
					PlayerCard[i].GetComponent<Image>().sprite = Sprites;
				}
				for (int i = 4; i < 8; i++) // fills PC cards with the same name sprites
				{
					Sprites = Resources.Load<Sprite>("Cards\\" + SuffleCards.cards[HandCount + i].ToString());
					PCCard[i - 4].GetComponent<Image>().sprite = Sprites;
					//Sprites = Resources.Load<Sprite>("red_back");
					//PCCard[i].GetComponent<Image>().sprite = Sprites;
				}
			}

			if (OpponentControl.PCMove == 4) // Pc is the last one who plays the hand so I sety codes on pc moves
			{
				HandCount += 8; // hand encrese 8 couse I give 4 cards to player and 4 cards to pc
				for (int i = 0; i < 4; i++)
				{
					PlayerCard[i].transform.Translate(230, -280, 0); // same animation

				}
				for (int i = 0; i < 4; i++)
				{
					PCCard[i].transform.Translate(230, 280, 0);

				}
				OpponentControl.PCMove = 0;
				IsStarted = true;
				Sprites = Resources.Load<Sprite>("Cards\\" + CurrentCardName); // and the top card gets the same Sprite which has been throwen
				CurrentCard.GetComponent<Image>().sprite = Sprites;
			}
		}
		//else
		//	GameTxt.text = "GAME OVER";

	}

	public void StartGame(Button btn)
	{

		IsStarted = true;
		btn.gameObject.SetActive(false);

		Sprites = Resources.Load<Sprite>("Cards\\" + SuffleCards.cards[11].ToString()); // fills the current cards Sprite in the beging
		CurrentCard.GetComponent<Image>().sprite = Sprites;
		for (int i = 0; i < 12; i++)
		{
			ScoreControl.CardsOnTable.Add(SuffleCards.cards[i].ToString());  // gest 12 card in the begining 1 for current card 3 is closed cards 4 cards for player 4 cards for pc
			
		}
		DebugText.text += "Player Cards:\n";
		for (int i = 0; i < 4; i++)
		{
			DebugText.text+= SuffleCards.cards[i].ToString() + "|";
		}
		DebugText.text += "\nPC Cards:\n";
		for (int i = 4; i < 8; i++)
		{
			DebugText.text += SuffleCards.cards[i].ToString() + "|";
		}
		DebugText.text += "\nPile Cards:\n";
		for (int i = 8; i < 12; i++)
		{
			DebugText.text += SuffleCards.cards[i].ToString() + "|";
		}

	}
}


