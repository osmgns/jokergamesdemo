﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// Here is Where PC controls were set up

public class OpponentControl : MonoBehaviour
{

	public Button CurrentCard;   // the card is open in th middle
	public Button[] Btn;         // Pc Cards
	public Button[] BtnBacks;    // pc cards are closed so player cant see them
	public Button Top;           // this is the card that on the top has white sprite and gets top nwhen some one win 
   
	public static int PCMove = 0;  // this controls how many time Pc has played
	int CardCounter;               // this helps that How many card Pc throwed  at the moment

 
	private void Start()
	{

		CardCounter = 0; // in the start no one has card and no one has played
		PCMove = 0;
	}

	private void Update()
	{

		if (PlayerControl.Turn == false && CardCounter <= 3) // if turn is false than pc plays
		{

			Btn[CardCounter].transform.position = CurrentCard.transform.position; // When Pc plays the card goes the current cards position and is child as well
			Btn[CardCounter].transform.parent = CurrentCard.transform;
			PlayerControl.Turn = true;   // and turns goes to player yet thşis fonksion stil continiues
			if (PlayerControl.CurrentCardValue == Btn[CardCounter].GetComponent<Image>().sprite.name[0]) // here is if pc played right and scored
			{                                                                                     // I get the first string of the card this is how I manage if the cards matches or not
				if (PlayerControl.CurrentCardValue == 'J') // if pc makes pişti with Jack
				{
					ScoreControl.PCScore += 20;
					foreach (var item in ScoreControl.CardsOnTable)
					{
						ScoreControl.PCEarnedCards.Add(item);             // it adds all cards to pc deck
					}
					ScoreControl.CardsOnTable.Clear();                   // and the cards on tbel goes null
					ScoreControl.LastEarned = false;                // if last earnd is false then pc gets all cards left 
				}
				else
				{
					ScoreControl.PCScore += 10;           // if pc makes pişti without Jack this works
					foreach (var item in ScoreControl.CardsOnTable)
					{
						ScoreControl.PCEarnedCards.Add(item);
					}
					ScoreControl.CardsOnTable.Clear();
					ScoreControl.LastEarned = false;
				}
				Top.gameObject.SetActive(true);
				Top.gameObject.transform.SetSiblingIndex(CurrentCard.transform.childCount);  // this makes the current card on tje top so plyer cant see any card
			}
			else if (Btn[CardCounter].GetComponent<Image>().sprite.name[0] == 'J') // if PC earns cards with only Jack
			{

				foreach (var item in ScoreControl.CardsOnTable)
				{
					ScoreControl.PCEarnedCards.Add(item);
				}
				ScoreControl.CardsOnTable.Clear();
				ScoreControl.LastEarned = false;           
				Top.gameObject.SetActive(true);
				Top.gameObject.transform.SetSiblingIndex(CurrentCard.transform.childCount);



			}
			else          // if pc ards doesnt match with current card
			{
				PlayerControl.CurrentCardValue = Btn[CardCounter].GetComponent<Image>().sprite.name[0];
				DistributeCards.CurrentCardName = Btn[CardCounter].GetComponent<Image>().sprite.name;
				ScoreControl.CardsOnTable.Add(DistributeCards.CurrentCardName);
				Top.gameObject.SetActive(false);  // in this case top card gose inactive and plyer sees the card on the top
				BtnBacks[CardCounter].gameObject.SetActive(false); // and the card covers pc cards goes inactive aswell

			}
			CardCounter++;   // card counter and move increase
			PCMove++;
			if (PCMove == 4)  // if pc playes his 4 cards then his cards refreshes and again comes with close
			{
				CardCounter = 0;
				for (int i = 0; i < 4; i++)
				{
					BtnBacks[i].gameObject.SetActive(true); 
				}
			}
		}
	}
}
